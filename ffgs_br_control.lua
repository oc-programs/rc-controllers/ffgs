timers = {reactor=0, UPS=0}
ups_side = require("sides").right
ups_adapter_addr = "83409557-0d80-488a-9eeb-b71997dc6994"
capacitor_addr="7e8f212d-8320-48d4-a02c-c545c7c54a43"

function start(args)
   local stop_level = 0.95
   local start_level = 0.1
   local reactor_interval = 4
   local ups_interval = 10
   local cmp = require("component")
   local event = require("event")
   local ups_controller_addr = "d2cb4774-457a-469d-9bf4-9d3b720d5c50"
   local ups_controller = cmp.proxy(ups_controller_addr)
   local capacitor = cmp.proxy(capacitor_addr)
   if ups_controller then
      for i = 0, 5 do ups_controller.setOutput(i, 0) end
   end

   local function setReactorControl()
      local br = cmp.br_reactor
      if br then
         -- local capacity = br.getEnergyCapacity()
         -- local level = br.getEnergyStored() / r_capacity
         local e_capacity = capacitor.getMaxEnergyStored()
         local e_level = capacitor.getEnergyStored() / e_capacity
         if e_level > stop_level then
            br.setActive(false)
         end
         if e_level < start_level then
            br.setActive(true)
         end
      end
   end
   
   local function setUPSControl()
      local ups = cmp.proxy(ups_adapter_addr)
      if ups and ups_controller then
         local ups_capacity = ups.getMaxEnergyStored()
         local u_level = ups.getEnergyStored() / ups_capacity
         if u_level > stop_level then
            ups_controller.setOutput(ups_side, 0)
         end
         if u_level < start_level then
            ups_controller.setOutput(ups_side, 15)
         end
      end
   end

   timers.reactor = event.timer(reactor_interval, setReactorControl, math.huge)
   timers.UPS = event.timer(ups_interval, setUPSControl, math.huge)
end

function stop()
   local event = require("event")
   event.cancel(timers.reactor)
   event.cancel(timers.UPS)
   require("component").br_reactor.setActive(false) -- shutdown reactor
   require("component").proxy(ups_controller_addr).setOutput(ups_side, 0)
end
