timers = {reactor=0}


function start(args)
   local stop_level = 0.82
   local start_level = 0.1
   local reactor_interval = 4
   local cmp = require("component")
   local event = require("event")

   local function setReactorControl()
      local br = cmp.br_reactor
      if br then
         local r_capacity = 10000000
         local r_level = br.getEnergyStored() / r_capacity
         if r_level > stop_level then
            br.setActive(false)
         end
         if r_level < start_level then
            br.setActive(true)
         end
      end
   end

   timers.reactor = event.timer(reactor_interval, setReactorControl, math.huge)
end

function stop()
   local event = require("event")
   event.cancel(timers.reactor)
   require("component").br_reactor.setActive(false) -- shutdown reactor
end