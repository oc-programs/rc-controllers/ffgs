timers = {reactor=0}
capacitor_addr="783d60ff-95cf-41a6-8249-3bbfd273fbb3"

function start()
   local stop_level = 0.95
   local start_level = 0.1
   local fuel_warning_level = 0.9
   local reactor_interval = 4
   local cmp = require("component")
   local event = require("event")
   local capacitor = cmp.proxy(capacitor_addr)
   local lamp_redstone = cmp.redstone

   local function setReactorControl()
      local br = cmp.br_reactor
      if br then
         -- local capacity = br.getEnergyCapacity()
         -- local level = br.getEnergyStored() / r_capacity
         local e_capacity = capacitor.getMaxEnergy()
         local e_level = capacitor.getEnergy() / e_capacity
         if e_level > stop_level then
            br.setActive(false)
         end
         if e_level < start_level then
            br.setActive(true)
         end

         local fuel_stats=br.getFuelStats()
         if fuel_stats.fuelAmount / fuel_stats.fuelCapacity < fuel_warning_level then
            for i = 0, 5 do lamp_redstone.setOutput(i, 15) end
         else
            for i = 0, 5 do lamp_redstone.setOutput(i, 0) end
         end
      end
   end
   timers.reactor = event.timer(reactor_interval, setReactorControl, math.huge)
end

function stop()
   local event = require("event")
   event.cancel(timers.reactor)
   require("component").br_reactor.setActive(false) -- shutdown reactor
end
