timers_me = {disabler=nil}
me_control = "f7cebdd0-5868-4bf4-8b36-1cdecad6cc53"

function start(args)
   local me_control_side = require("sides").top
   local valid_users = {["man_cubus"] = true, ["nikki"] = true, ["xhilatreae"] = true, ["sanovskiy"] = true}
   local sensitivity = 64
   local me_off_interval = 300 --5 minutes
   local cmp = require("component")
   local event = require("event")
   local red_on = 0
   local red_off = 15
   for k, v in cmp.list() do
      if v == "motion_sensor" then
         cmp.proxy(k).setSensitivity(sensitivity)
      end
   end
   cmp.proxy(me_control).setOutput(me_control_side, red_on)

   function timers_me.me_off()
      cmp.proxy(me_control).setOutput(me_control_side, red_off)
   end

   local function reset_off_timer()
      if timers_me.disabler then
         event.cancel(timers_me.disabler)
      end
      timers_me.disabler = event.timer(me_off_interval, timers_me.me_off, math.huge)
   end

   function timers_me.me_on()
      cmp.proxy(me_control).setOutput(me_control_side, red_on)
      reset_off_timer()
   end

   function timers_me.motion_handler(motion, ...)
      local evt = {...}
      if valid_users[evt[5]] then
         timers_me.me_on()
      end
   end

   function timers_me.tunnel_handler(modem_message, ...)
      local evt = {...}
      local valid_tunnel = (cmp.proxy(evt[1]).type == "tunnel")
      if valid_tunnel then
         if evt[5] == "me_on" then
            timers_me.me_on()
         elseif evt[5] == "me_off" then
            timers_me.me_off()
         end
      end
   end

   event.listen("modem_message", timers_me.tunnel_handler)
   event.listen("motion", timers_me.motion_handler)
   reset_off_timer()
end

function stop()
   local event = require("event")
   event.cancel(timers_me.disabler)
   event.ignore("modem_message", timers_me.tunnel_handler)
   event.ignore("motion", timers_me.motion_handler)
   timers_me.me_on()
end

function off()
   require("component").proxy(me_control).setOutput(require("sides").top, 15)
end