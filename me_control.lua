local args = {...}
local cmp = require("component")
local usage = [[me on - turns ME on
me off - turns ME off]]

if args[1] then
  if args[1] == "on" then
    cmp.tunnel.send("me_on")
  elseif args[1] == "off" then
    cmp.tunnel.send("me_off")
  else
    print(usage)
  end
else
  print(usage)
end